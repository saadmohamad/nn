import torch
import os
import math
import itertools
from multiprocessing import pool
import functools
import numpy as np

from pre_processing import *
def train_valid_loaders(dataset, valid_fraction,test_fraction, **kwargs):
    num_train = len(dataset)
    indices = list(range(num_train))
    split = int(math.floor((valid_fraction+test_fraction)* num_train))

    if not('shuffle' in kwargs and not kwargs['shuffle']):
            #np.random.seed(random_seed)
            np.random.shuffle(indices)
    if 'num_workers' not in kwargs:
        kwargs['num_workers'] = 1
    valid_split=math.floor((valid_fraction/(valid_fraction+test_fraction))*split)
    train_idx, valid_idx,test_idx = indices[split:], indices[:valid_split],indices[valid_split:split],
    train_sampler = torch.utils.data.SubsetRandomSampler(train_idx)
    valid_sampler = torch.utils.data.SubsetRandomSampler(valid_idx)
    test_sampler = torch.utils.data.SubsetRandomSampler(test_idx)

    train_loader = torch.utils.data.DataLoader(dataset,
                                               sampler=train_sampler,
                                               batch_size=kwargs['batch_size'],
                                               num_workers=kwargs['num_workers']
                                               )
    valid_loader = torch.utils.data.DataLoader(dataset,
                                               sampler=valid_sampler,
                                               batch_size=64,
                                               num_workers=kwargs['num_workers'])
    test_loader = torch.utils.data.DataLoader(dataset,
                                               sampler=test_sampler,
                                               batch_size=1,
                                               num_workers=kwargs['num_workers'])
    return train_loader, valid_loader,test_loader

def parallel_indexing(frame_id,cells,frame,sequ_len):
    gab = 6
    index=[]
    relevant_index = [np.where(frame[:] == j)[0] for j in range(frame_id, frame_id + gab * sequ_len, gab)]
    for j in relevant_index[0]:
        cell_id = cells[j]
        cell_ind = [cell_id, [j]]
        # search for in same cell_id in next frame
        for jj in range(1, sequ_len):
            nex_frame_within_relevant = np.where(cells[relevant_index[jj]] == cell_id)[0]
            if (len(nex_frame_within_relevant) == 0):
                break
            cell_ind[1].append(relevant_index[jj][nex_frame_within_relevant[0]])
        index.append(cell_ind)
    return index

def create_index_for_sequnece(cells, frame, sequ_len,nb_workers=15):
    unique_frames = np.unique(frame[:])
    func = functools.partial(parallel_indexing,cells=cells,frame=frame,sequ_len=sequ_len)
    p=pool.Pool(nb_workers)
    index=p.map(func, unique_frames[:-sequ_len])
    p.close()
    p.join()
    index = list(itertools.chain(*index))
    return index
class Dataset(torch.utils.data.Dataset):
    def __init__(self, filename,sequences=True,seqlen=2):
        h5_file=filename+'.h5' if(not sequences) else filename + 'seq.h5'
        if(not os.path.exists(h5_file)):
            print('creating h5 data file')
            creat_h5_datasets(filename,sequences)
            print('h5 data file is created')
        h5f = h5py.File(h5_file, 'r')
        data= h5f['features'][:,:]
        self.mask = np.ones(data.shape[1], dtype=bool)
        not_taken=np.where((np.isinf(data.max(axis=0))))[0]
        self.mask[not_taken]=0

#

        print('removing '+str(len(self.mask)-sum(self.mask))+' columns')
#
        self.seqlen=seqlen
        self.mean=data[:,self.mask].mean(axis=0)
        self.std=data[:,self.mask].std(axis=0)
        self.data=h5f['features']
        self.labels=h5f['labels']
        if(sequences):
            print('Loading sequences indeces')
            self.inds=create_index_for_sequnece(h5f['Cell_id'][:],h5f['frame'][:],seqlen)
            print('Loading is done')
        self.seq=sequences
        self.data=h5f['features'][:,:]
        self.labels=h5f['labels'][:]







    def __len__(self):
        if(self.seq):
            return len(self.inds)
        return self.data.shape[0]

    def __getitem__(self, index):

       if(self.seq):
            to_fill=self.seqlen-len(self.inds[index][1])
            y=[ self.labels[i]-1 for i in self.inds[index][1]]

            x = [ (self.data[i][self.mask] - self.mean) / self.std for i in  self.inds[index][1]]

            return torch.cat((torch.tensor(x),torch.zeros((to_fill,x[0].shape[0]))) ), torch.cat((torch.tensor(y),torch.full((to_fill,),-1,dtype=torch.long)) )
       y=self.labels[index]-1
       x=(self.data[index][self.mask]-self.mean)/self.std
       return torch.tensor(x), torch.tensor(y)

