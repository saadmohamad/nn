import argparse
import numpy as np
import torch
torch.manual_seed(0)
np.random.seed(0)
import torch.utils.data
import matplotlib.pyplot as plt
from Networks import *
from dataloader import *
import torch.nn as nn
from sklearn.metrics import classification_report,confusion_matrix

def main(args):



    device = torch.device('cuda' if args.device=='cuda' else 'cpu')
    hidden_size = args.hidden_size
    dimention=args.dimention

    data_path=args.in_data_path
    embedding=args.out_embedding_folder
    dataset=Dataset(data_path,args.LSTM,args.sequ_len)
    num_classes=4
    input_size = int(sum(dataset.mask))

    model = NeuralNet(input_size, hidden_size, num_classes, 2, args.groups_centers,False,args.LSTM, args.LSTM_layers).to(device)
    dict1 = torch.load(args.load_model)
    model.load_state_dict(dict1['model_state'])
    dataset.mean=dict1['normalisation_param']['mean'] #check
    dataset.std = dict1['normalisation_param']['std'] #check
    test_loader = torch.utils.data.DataLoader(dataset,
                                              batch_size=1,shuffle=True)

    if(args.record_testing_embedding):
        print('Start recoding the testing embedding')
        i=0
        with open(embedding, 'w') as f:
            with torch.no_grad():
                for samples, labels in test_loader:
                    i += 1
                    #if(i>5000):
                    #    break
                    samples = samples.to(device)
                    if (args.LSTM):
                        masks = labels.view(-1) != -1
                        labels = labels.view(-1)[masks]
                        outputs = model(samples, hidden=True, sequ=args.LSTM, take=masks).cpu().numpy()[0]
                        prob = nn.Softmax()(model(samples, hidden=False, sequ=args.LSTM, take=masks)).cpu().numpy()
                        for lab, pro in zip(labels,prob):
                            line = ','.join(
                                [str(outputs[i]) for i in range(dimention)] + [str(lab.item())] + [str(i) for i in pro]) + '\n'
                            f.write(line)
                    else:
                        outputs = model(samples, hidden=True).cpu().numpy()[0]
                        prob = nn.Softmax()(model(samples, hidden=False)).cpu().numpy()[0]
                        labels = labels.item()
                        line = ','.join(
                            [str(outputs[i]) for i in range(dimention)] + [str(labels)] + [str(i) for i in prob]) + '\n'
                        f.write(line)
    print('getting quantitative: ')
    if(args.qunatitative):
        predicted_labl=[]
        correct=[]
        test_loader2 = torch.utils.data.DataLoader(dataset,
                                                  batch_size=128, shuffle=True,num_workers=8)
        with torch.no_grad():
            for samples, labels in test_loader2:
                samples = samples.to(device)
                #ind=np.where(labels!=3)
                #predicted_labl.extend(torch.argmax(torch.nn.Softmax(dim=1)(model(samples, hidden=False, sequ=args.LSTM).cpu()[:,:-1]),dim=1).numpy()[ind])
                predicted_labl.extend(torch.argmax(torch.nn.Softmax(dim=1)(model(samples, hidden=False, sequ=args.LSTM).cpu()[:,:]),dim=1).numpy())
                #correct.extend(labels.numpy()[ind])
                correct.extend(labels.numpy())
        target_names = ['Pluri', 'Ecto', 'Endo','Meso']
        print(classification_report(correct, predicted_labl, target_names=target_names))
        del test_loader2
    print('plotting testing samples')
    if (args.plot_sample):
        if (dimention == 2):
            fig, ax = plt.subplots()
            cdict = {0: 'red', 1: 'blue', 2: 'green', 3: 'olive',4:'cyan',5:'yellow',6:'magenta',7:'black'}
            count = [0,0,0,0,0,0,0]
            maxim=5000
            with torch.no_grad():
                for samples, labels in test_loader:
                    samples = samples.to(device)
                    if (args.LSTM):
                        masks = labels.view(-1) != -1
                        labels = labels.view(-1)[masks]
                        outputs = model(samples, hidden=True, sequ=args.LSTM, take=masks).cpu().numpy()
                        if (sum(count) >= maxim * 3):
                            break
                        for out,lab in zip(outputs,labels):
                            if (count[lab] >= maxim):
                                continue
                            count[lab]+=1
                            ax.scatter(out[0], out[1], c=cdict[lab.item()], label=lab.item(), s=100)
                    else:
                        labels = labels.item()
                        if (sum(count) >= maxim * 3):
                            break
                        if (count[labels] >= maxim):
                            continue
                        count[labels] += 1
                        outputs = model(samples, hidden=True).cpu().numpy()[0]
                        ax.scatter(outputs[0], outputs[1], c=cdict[labels], label=labels, s=100)
            plt.show()
        else:
            print('can not plot')



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--device", type=str, default='cuda',
                        help='cuda for gpu or cpu')
    parser.add_argument('--hidden_size', type=int, default=512, help="dimention of images")
    parser.add_argument('--dimention', type=int, default=2, help="embedding dimentions")
    parser.add_argument("--groups_centers", nargs="*", type=float, default=[[0,-1],[1,0], [-1,0],[0,0]],
                        help="centers of embedding groups")
    parser.add_argument('--LSTM', default=False, action='store_true')
    parser.add_argument("--LSTM_layers", type=int, default=2, help="length of training sequ")
    parser.add_argument("--sequ_len", type=int, default=10, help="length of training sequ")
    parser.add_argument("--in_data_path", type=str, default='/home/sm20/Ed_project_data/Dataset2PluriEndoEctoMeso.csv',
                        help='path to the training validation and testing data') #Dataset3InterpretableFeaturesPluriEctoEndoMesoOrder    #7ConditionDatasetInterpretableFeatures4Conditio
    parser.add_argument("--out_features_imp", type=str, default='/home/sm20/Ed_project_data/features.csv',
                        help='path to the output features werights')
    parser.add_argument("--out_embedding_folder", type=str, default='/home/sm20/Ed_project_data/2d_embedding.csv',
                        help='path to the output embedding')
    parser.add_argument('--plot_sample', default=True, action='store_true')
    parser.add_argument('--record_testing_embedding', default=False, action='store_true')
    parser.add_argument('--qunatitative', default=False, action='store_true')
    parser.add_argument("--load_model", type=str, default='./m.ckpt',
                        help='path to the output embedding')

    args = parser.parse_args()

    main(args)
