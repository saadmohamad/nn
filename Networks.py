import torch
import torch.nn as nn

class NeuralNet(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes,dimention,centers,assignment,LSTM=False,lstm_layers=1):
        super(NeuralNet, self).__init__()
        self.fc0=nn.Linear(input_size, hidden_size) if(not LSTM) else nn.LSTM(input_size,hidden_size,lstm_layers,batch_first=True)
        self.fc1 = nn.Linear(hidden_size, int(hidden_size/2))
        self.fc11 = nn.Linear(int(hidden_size/2), int(hidden_size / 4))
        self.fc12 = nn.Linear(int(hidden_size/4), dimention)
        self.relu = nn.ReLU()
        self.num_classes=num_classes
        self.fc2 = nn.Linear(dimention, num_classes)
        #self.fc2.weight.data.fill_(1)
        for i in range(num_classes):
            for j in range(dimention):
                self.fc2.weight.data[i, j] = centers[i][j]
        self.fc2.weight.requires_grad = not assignment
        self.fc2.bias.data.fill_(1)

    def forward(self, x,hidden=False,sequ=False,take=None):
        if(sequ):
            out,_=self.fc0(x)
            out=out.contiguous().view(-1, 1, self.fc1.in_features).squeeze()[take]

        else:
            out=self.fc0(x)

        out=self.relu(out)
        out = self.fc1(out)
        out = self.relu(out)
        out=self.fc11(out)
        out = self.relu(out)
        out=self.fc12(out)
        out = out
        if(hidden):
            return out
        #out = self.fc2(out)
        #out=torch.exp(-torch.stack(
        #    [torch.sum((out - self.fc2.weight[i]) ** 2, dim=1) ** 0.5 for i in range(self.num_classes)], dim=1))*self.fc2.bias[0]

        out=-nn.Tanhshrink()(torch.stack(
            [torch.sum((out - self.fc2.weight[i]) ** 2, dim=1) ** 0.5 for i in range(self.num_classes)], dim=1)*4)

        #out=-nn.Softshrink(1.5)(torch.stack(
        #    [torch.sum((out - self.fc2.weight[i]) ** 2, dim=1) ** 0.5 for i in range(self.num_classes)], dim=1)*20)

        #out=(1-1/(1+torch.exp(-(self.fc2.bias[0]**2)*torch.stack(
        #    [torch.sum((out - self.fc2.weight[i]) ** 2, dim=1) ** 0.5 for i in range(self.num_classes)], dim=1))))*10
        #print(self.fc2.bias[0])
        return out


