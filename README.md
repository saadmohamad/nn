### What is this repository for? ###

 This repository is for the embedding and prediction model, please refer to Figure 2 in the paper

### Requirement ###

- pytorch                   1.0.0
- numpy                     1.18.1
- scikit-learn              0.23.2
- pandas                    1.1.4
- h5py                      3.0.0 

